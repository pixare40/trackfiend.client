﻿using System;
namespace Trackfiend.Client.Infrastructure.Models.Account
{
    public class RegistrationResponse
    {
        public RegisteredPerson Account { get; set; }
    }

    public class RegisteredPerson
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}
