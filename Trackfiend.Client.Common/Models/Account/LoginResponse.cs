﻿namespace Trackfiend.Client.Common.Models.Account
{
    public class LoginResponse
    {
        public LoginToken Login { get; set; }
    }

    public class LoginToken
    {
        public string Token { get; set; }
    }
}
