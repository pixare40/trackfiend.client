﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Core.Common.Contracts;
using Xamarin.Forms.Internals;

namespace Trackfiend.Client.Infrastructure.Models
{
    [Preserve(AllMembers = true)]
    public class Artist : INotifyPropertyChanged, IIdentifiableResource
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public int Id { get; set; }

        public string Name { get; set; }

        public string Realname { get; set; }

        public string Cname { get; set; }

        public string ArtistImageUrl { get; set; }

        public string Bio { get; set; }

        public string Mbid { get; set; }

        public string Discogsid { get; set; }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
