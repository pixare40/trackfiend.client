﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Core.Common.Contracts;
using Xamarin.Forms.Internals;

namespace Trackfiend.Client.Infrastructure.Models
{
    /// <summary>
    /// Model for Article templates.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class Article : INotifyPropertyChanged, IIdentifiableResource
    {
        private bool isBookmarked;

        public event PropertyChangedEventHandler PropertyChanged;

        public int Id { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public string Url { get; set; }

        public string Summary { get; set; }

        public string ArticleTypeId { get; set; }

        public DateTime InsertedAt { get; set; }

        public DateTime UpdatedAt{ get; set; }

        public string ArticlePicture { get; set; }

        public string ImagePath { get; set; }

        public string Name { get; set; }

        public string Author { get; set; }

        public string Date { get; set; }

        public string AverageReadingTime { get; set; }

        public string Description { get; set; }

        public bool IsBookmarked
        {
            get
            {
                return isBookmarked;
            }

            set
            {
                isBookmarked = value;
                NotifyPropertyChanged();
            }
        }

        public void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}