﻿using Prism.Ioc;
using Prism.Modularity;

namespace Trackfiend.Client.Infrastructure
{
    public class CommonModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}
