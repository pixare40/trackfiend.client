﻿using System;
using GraphQL;
using Prism.Events;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Trackfiend.Client.Infrastructure.Models;
using Trackfiend.Client.Infrastructure;
using Trackfiend.Client.Infrastructure.Events;
using Trackfiend.Client.Infrastructure.Services;
using Trackfiend.Client.Infrastructure.ValueObjects.Artists;

namespace Trackfiend.Client.Modules.Artists.Services
{
    public class ArtistService
    {
        private ICommunicationsService communicationsService;
        private IEventAggregator eventAggregator;

        public ArtistService(ICommunicationsService communicationsService, IEventAggregator eventAggregator)
        {
            this.communicationsService = communicationsService;
            this.eventAggregator = eventAggregator;
        }

        public async void GetArtists()
        {
            var response = await communicationsService
                .SendQueryAsync<object>(new ArtistsRequest().GetArtists);
            if (response.Errors != null)
            {
                eventAggregator.GetEvent<ArtistsEventError>().Publish(response.Errors);
                return;
            }

            string responseData = response.Data.ToString();
            var dataReturned = JsonConvert.DeserializeObject<Dictionary<string, List<Artist>>>(responseData)
                .FirstOrDefault();

            List<Artist> artists = dataReturned.Value;

            eventAggregator.GetEvent<ArtistsEvent>().Publish(artists);
        }

        public async void GetArtist(int id)
        {
            var response = await communicationsService.SendQueryAsync<object>(new ArtistsRequest().GetArtist(id));
            if (response.Errors != null)
            {
                eventAggregator.GetEvent<ArtistsEventError>().Publish(response.Errors);
                return;
            }

            string responseData = response.Data.ToString();
            var dataReturned = JsonConvert.DeserializeObject<Dictionary<string, Artist>>(responseData).FirstOrDefault();
            Artist artist = dataReturned.Value;

            eventAggregator.GetEvent<ArtistEvent>().Publish(artist);
        }

        public async void SearchArtist(string searchTerm)
        {
            var response =
                await communicationsService.SendQueryAsync<object>(new ArtistsRequest().SearchArtist(searchTerm));
            if (response.Errors != null)
            {
                eventAggregator.GetEvent<ArtistsEventError>().Publish(response.Errors);
                return;
            }

            string responseData = response.Data.ToString();
            var dataReturned = JsonConvert.DeserializeObject<Dictionary<string, List<Artist>>>(responseData)
                .FirstOrDefault();

            List<Artist> artists = dataReturned.Value;

            eventAggregator.GetEvent<ArtistsEvent>().Publish(artists);
        }
    }
}
