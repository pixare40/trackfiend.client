﻿using Prism.Ioc;
using Prism.Modularity;
using Trackfiend.Client.Modules.Artists.Views;
using Trackfiend.Client.Modules.Artists.ViewModels;

namespace Trackfiend.Client.Modules.Artists
{
    public class ArtistsModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<ViewA, ViewAViewModel>();
        }
    }
}
