﻿namespace Core.Common.Contracts
{
    public interface IIdentifiableResource
    {
        int Id { get; set; }
    }
}
