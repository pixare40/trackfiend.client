﻿using GraphQL;
using Newtonsoft.Json;
using Prism.Events;
using System.Collections.Generic;
using System.Linq;
using Trackfiend.Client.Infrastructure.Models;
using Trackfiend.Client.Infrastructure.Events;
using Trackfiend.Client.Infrastructure.Services;
using Trackfiend.Client.Infrastructure.ValueObjects.Articles;

namespace Trackfiend.Client.Modules.Articles.Services
{
    public class ArticlesService : IArticlesService
    {
        private readonly ICommunicationsService communicationsService;
        private readonly IEventAggregator eventAggregator;

        public ArticlesService(ICommunicationsService communicationsService, IEventAggregator eventAggregator)
        {
            this.communicationsService = communicationsService;
            this.eventAggregator = eventAggregator;
        }

        public async void GetArticles()
        {
            GraphQLResponse<object> response = await communicationsService
                .SendQueryAsync<object>(new ArticlesRequest().GetArticles);
            if (response.Errors != null)
            {
                eventAggregator.GetEvent<ArticlesEventError>().Publish(response.Errors);
                return;
            }

            string responseData = response.Data.ToString();
            KeyValuePair<string, List<Article>> datareturned = JsonConvert
                .DeserializeObject<Dictionary<string, List<Article>>>(responseData)
                .FirstOrDefault();

            List<Article> articles = datareturned.Value; ;

            eventAggregator.GetEvent<ArticlesEvent>().Publish(articles);
        }
    }
}
