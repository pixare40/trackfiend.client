﻿namespace Trackfiend.Client.Modules.Articles.Services
{
    public interface IArticlesService
    {
        void GetArticles();
    }
}