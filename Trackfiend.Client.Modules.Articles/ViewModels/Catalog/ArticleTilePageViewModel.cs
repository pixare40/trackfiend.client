﻿using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Trackfiend.Client.Infrastructure.Models;
using Trackfiend.Client.Infrastructure.ViewModels;
using Trackfiend.Client.Infrastructure.Events;
using Trackfiend.Client.Infrastructure.ValueObjects.Articles;
using Trackfiend.Client.Modules.Articles.Services;
using Xamarin.Forms.Internals;

namespace Trackfiend.Client.Modules.Articles.ViewModels.Catalog
{
    /// <summary>
    /// ViewModel for article list page.
    /// </summary> 
    [Preserve(AllMembers = true)]
    public class ArticleTilePageViewModel : ViewModelBase
    {
        #region Fields

        private ObservableCollection<Article> featuredStories;

        private ObservableCollection<Article> latestStories;
        private readonly ArticlesService articlesService;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the property that has been bound with the rotator view, which displays the articles featured stories items.
        /// </summary>
        public ObservableCollection<Article> FeaturedStories
        {
            get
            {
                return this.featuredStories;
            }

            set
            {
                if (this.featuredStories == value)
                {
                    return;
                }

                SetProperty(ref featuredStories, value);
            }
        }

        /// <summary>
        /// Gets or sets the property that has been bound with the list view, which displays the articles latest stories items.
        /// </summary>
        public ObservableCollection<Article> LatestStories
        {
            get
            {
                return this.latestStories;
            }

            set
            {
                if (this.latestStories == value)
                {
                    return;
                }

                SetProperty(ref latestStories, value);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance for the <see cref="ArticleTilePageViewModel" /> class.
        /// </summary>
        public ArticleTilePageViewModel(IEventAggregator eventAggregator, ArticlesService articlesService)
            : base(eventAggregator)
        {
            articlesService.GetArticles();
            this.FeaturedStories = new ObservableCollection<Article>
            {
                new Article
                {
                    ImagePath = "", //App.BaseImageUrl + "ArticleImage2.png",
                    Name = "Learning to Reset",
                    Author = "John Doe",
                    Date = "Aug 2010",
                    AverageReadingTime = "5 mins read"
                },
                new Article
                {
                    ImagePath = "", // App.BaseImageUrl + "ArticleImage3.png",
                    Name = "Holistic Approach to UI Design",
                    Author = "John Doe",
                    Date = "Apr 16",
                    AverageReadingTime = "5 mins read"
                },
                new Article
                {
                    ImagePath = "",//App.BaseImageUrl + "ArticleImage4.png",
                    Name = "Guiding Your Flock to Success",
                    Author = "John Doe",
                    Date = "May 2012",
                    AverageReadingTime = "5 mins read"
                },
                new Article
                {
                    ImagePath = "",// App.BaseImageUrl + "ArticleImage5.png",
                    Name = "Be a Nurturing, Fierce Team Leader",
                    Author = "John Doe",
                    Date = "Apr 16",
                    AverageReadingTime = "5 mins read"
                },
                new Article
                {
                    ImagePath = "",//App.BaseImageUrl + "ArticleImage6.png",
                    Name = "Holistic Approach to UI Design",
                    Author = "John Doe",
                    Date = "Dec 2013",
                    AverageReadingTime = "5 mins read"
                }
            };

            this.LatestStories = new ObservableCollection<Article>
            {
                new Article
                {
                    ImagePath = "",//App.BaseImageUrl + "Article_image1.png",
                    Name = "Learning to Reset",
                    Author = "John Doe",
                    Date = "Apr 16",
                    AverageReadingTime = "5 mins read"
                },
                new Article
                {
                    ImagePath = "",//App.BaseImageUrl + "Article_image2.png",
                    Name = "Holistic Approach to UI Design",
                    Author = "John Doe",
                    Date = "May 26",
                    AverageReadingTime = "5 mins read"
                },
                new Article
                {
                    ImagePath = "",//App.BaseImageUrl + "Article_image3.png",
                    Name = "Guiding Your Flock to Success",
                    Author = "John Doe",
                    Date = "Apr 10",
                    AverageReadingTime = "5 mins read"
                },
                new Article
                {
                    ImagePath = "",//App.BaseImageUrl + "Article_image4.png",
                    Name = "Holistic Approach to UI Design",
                    Author = "John Doe",
                    Date = "Apr 16",
                    AverageReadingTime = "5 mins read"
                },
            };

            this.MenuCommand = new DelegateCommand<object>(this.MenuClicked);
            this.BookmarkCommand = new DelegateCommand<object>(this.BookmarkButtonClicked);
            this.FeatureStoriesCommand = new DelegateCommand<object>(this.FeatureStoriesClicked);
            this.ItemSelectedCommand = new DelegateCommand<object>(this.ItemSelected);
            this.articlesService = articlesService;
        }

        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the command that will be executed when the menu button is clicked.
        /// </summary>
        public DelegateCommand<object> MenuCommand { get; private set; }

        /// <summary>
        /// Gets or sets the command that will be executed when the bookmark button is clicked.
        /// </summary>
        public DelegateCommand<object> BookmarkCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that will executed when the feature stories item is clicked.
        /// </summary>
        public DelegateCommand<object> FeatureStoriesCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that will be executed when an item is selected.
        /// </summary>
        public DelegateCommand<object> ItemSelectedCommand { get; set; }

        #endregion

        #region Methods

        protected override void SubscribeEvents()
        {
            base.SubscribeEvents();

            EventAggregator.GetEvent<ArticlesEvent>().Subscribe(OnArticlesReceived);
        }

        private void OnArticlesReceived(object obj)
        {
            List<Article> articles = obj as List<Article>;

            Console.WriteLine("Articles received");
        }

        /// <summary>
        /// Invoked when the menu button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void MenuClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when the bookmark button is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private void BookmarkButtonClicked(object obj)
        {
            if (obj is Article article)
            {
                article.IsBookmarked = !article.IsBookmarked;
            }
        }

        /// <summary>
        /// Invoked when the the feature stories item is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private void FeatureStoriesClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when an item is selected.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void ItemSelected(object obj)
        {
            // Do something
        }

        #endregion
    }
}
