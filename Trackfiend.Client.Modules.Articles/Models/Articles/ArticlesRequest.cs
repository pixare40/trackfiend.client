﻿using System.Collections.Generic;
using GraphQL;

namespace Trackfiend.Client.Infrastructure.ValueObjects.Articles
{
    public class ArticlesRequest
    {
        public GraphQLRequest GetArticles =>
        new GraphQLRequest
        {
            Query = @"query{
                articles{
                    id
                    title
                    body
                    url
                    summary
                    article_type_id
                    artists{
                        name
                    }
                }
            }"
        };

        public GraphQLRequest GetArticle(int id) => new GraphQLRequest
        {
            Query = @"GetArticle($id: ID!){
            getArticle(id: $id){
                errors{key message}
                article{
                    id
                    title
                    body
                    url
                    summary
                    article_type_id
                    artists{
                        id
                        name
                        realname
                        cname
                        artistImageUrl
                        bio
                        mbid
                        discogsid
                    }
                }
            }",
            Variables = new Dictionary<string, int> { { "id", id} }
        };
    }
}
