﻿using System.Collections.Generic;
using Trackfiend.Client.Infrastructure.Models;
using Trackfiend.Client.Infrastructure;

namespace Trackfiend.Client.Modules.Articles.Models.Articles
{
    public class ArticlesResponse : BaseResponse<Article>
    {
        public ArticlesResponse(Dictionary<string, Article> fetchedData) : base(fetchedData)
        {
        }

        public override string Type { get => "article"; }
    }
}
