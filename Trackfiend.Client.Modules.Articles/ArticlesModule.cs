﻿using Prism.Ioc;
using Prism.Modularity;
using Trackfiend.Client.Modules.Articles.Views;
using Trackfiend.Client.Modules.Articles.ViewModels;

namespace Trackfiend.Client.Modules.Articles
{
    public class ArticlesModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<ViewA, ViewAViewModel>();
        }
    }
}
