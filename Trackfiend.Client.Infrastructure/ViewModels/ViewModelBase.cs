﻿using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using Trackfiend.Client.Infrastructure.Events;
using Xamarin.Forms;

namespace Trackfiend.Client.Infrastructure.ViewModels
{
    public abstract class ViewModelBase : BindableBase, IDisposable
    {
        protected IEventAggregator EventAggregator { get; private set; }
        protected INavigationService NavigationService { get; private set; }

        public ViewModelBase(IEventAggregator eventAggregator)
        {
            this.EventAggregator = eventAggregator;

            SubscribeEvents();
        }

        protected virtual void SubscribeEvents()
        {
            EventAggregator.GetEvent<ConnectionLostEvent>().Subscribe(OnConnectionLost, ThreadOption.UIThread);
        }

        private async void OnConnectionLost(object obj)
        {
            await Application.Current.MainPage
                .DisplayAlert("Connection Lost",
                "Connection with server has been lost please check your network connection",
                "Accept");
        }

        protected virtual void UnsubscribeEvents()
        {

        }

        public virtual void Dispose()
        {
            UnsubscribeEvents();
        }
    }
}
