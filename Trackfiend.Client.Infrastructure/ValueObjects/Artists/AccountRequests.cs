﻿using System;
using GraphQL;
using System.Collections.Generic;

namespace Trackfiend.Client.Infrastructure.ValueObjects.Artists
{
    public class AccountRequests
    {
        public GraphQLRequest Register(string name,
            string email, string password) => new GraphQLRequest()
            {
                Query = @"
                mutation Register($name: String!, $email: String!, $password: String!){
                    register(name: $name, email: $email, password: $password){
                        errors{key message}
                        account{id name email}
                    }
                }
                ",
                Variables = new
                {
                    name,
                    email,
                    password
                }
            };
    }
}
