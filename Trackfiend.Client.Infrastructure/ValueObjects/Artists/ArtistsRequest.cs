﻿using System.Collections.Generic;
using GraphQL;
using Trackfiend.Client.Infrastructure.Models;

namespace Trackfiend.Client.Infrastructure.ValueObjects.Artists
{
    public class ArtistsRequest
    {
        public GraphQLRequest GetArtists => new GraphQLRequest
        {
            Query = @"{
                    artists{
                      id
                      name
                      realname
                      cname
                      artistImageUrl
                      bio
                      mbid
                      discogsid
                    }
                  }"
        };

        public GraphQLRequest GetArtist(int id) => new GraphQLRequest()
        {
            Query = @"GetArtist($id: ID!){
              getArtist(id: $id){
                errors{key message}
                artist {
                  id
                  name
                  realname
                  cname
                  artist_image_url
                  bio
                  discogsid
                  mbid
                }
              }
            }",
            Variables = new
            {
                id
            }
        };

        public GraphQLRequest SearchArtist(string searchTerm) => new GraphQLRequest()
        {
            Query = @"SearchArtist($term: String!){
              searchArtist(matching: $term){
                id
                name
                realname
                cname
                artistImageUrl
                bio
                mbid
                discogsid
              }
            }",
            Variables = new
            {
                term = searchTerm
            }
        };
    }
}
