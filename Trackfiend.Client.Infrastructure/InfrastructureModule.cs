﻿using Prism.Ioc;
using Prism.Modularity;
using System;
using Trackfiend.Client.Infrastructure.Services;

namespace Trackfiend.Client.Infrastructure
{
    public class InfrastructureModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IImageService, ImageService>();
        }
    }
}
