﻿using System.Collections.Generic;

namespace Trackfiend.Client.Infrastructure
{
    public abstract class BaseResponse<T>
    {
        private Dictionary<string, T> data;

        public  T DataObject { get; }

        public abstract string Type { get; }

        public BaseResponse(Dictionary<string, T> fetchedData)
        {
            DataObject = fetchedData[Type];
        }
    }
}
