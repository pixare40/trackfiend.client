﻿using GraphQL;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using Prism.Events;
using System;
using System.Threading.Tasks;
using Trackfiend.Client.Infrastructure.Events;

namespace Trackfiend.Client.Infrastructure.Services
{
    public class CommunicationsService : ICommunicationsService
    {
        private readonly IGraphQLClient graphQlClient;
        private readonly IEventAggregator eventAggregator;

        public CommunicationsService(IEventAggregator eventAggregator)
        {
            graphQlClient = new GraphQLHttpClient("http://192.168.1.166:4000/api", new NewtonsoftJsonSerializer());
            this.eventAggregator = eventAggregator;
        }

        public async Task<GraphQLResponse<TResponse>> SendQueryAsync<TResponse>(GraphQLRequest request)
        {
            try
            {
                return await graphQlClient.SendQueryAsync<TResponse>(request);
            }
            catch(Exception e)
            {
                eventAggregator.GetEvent<ConnectionLostEvent>().Publish(e);
                return null;
            }
        }
    }
}
