﻿namespace Trackfiend.Client.Infrastructure.Services
{
    public interface IImageService
    {
        string GetUrl(string id);
    }
}
