﻿using GraphQL;
using GraphQL.Client.Http;
using System.Threading.Tasks;

namespace Trackfiend.Client.Infrastructure.Services
{
    public interface ICommunicationsService
    {
        Task<GraphQLResponse<TResponse>> SendQueryAsync<TResponse>(GraphQLRequest request);
    }
}