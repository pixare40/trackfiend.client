﻿using System;
using Prism.Events;

namespace Trackfiend.Client.Infrastructure.Events
{
    public class RegistrationFailedEvent : PubSubEvent<object[]>
    {
    }
}
