﻿using System;
using Prism.Events;
using Trackfiend.Client.Infrastructure.Models.Account;

namespace Trackfiend.Client.Infrastructure.Events
{
    public class RegistrationSuccessEvent : PubSubEvent<RegistrationResponse>
    {
    }
}
