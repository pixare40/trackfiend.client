﻿using Prism.Events;

namespace Trackfiend.Client.Infrastructure.Events
{
    public class ArtistsEventError : PubSubEvent<object>
    {
    }
}
