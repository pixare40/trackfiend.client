﻿using Prism.Events;

namespace Trackfiend.Client.Infrastructure.Events
{
    public class ArticlesEvent : PubSubEvent<object>
    {
    }
}
