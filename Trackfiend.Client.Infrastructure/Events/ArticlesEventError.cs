﻿using Prism.Events;

namespace Trackfiend.Client.Infrastructure.Events
{
    public class ArticlesEventError : PubSubEvent<object>
    {
    }
}
