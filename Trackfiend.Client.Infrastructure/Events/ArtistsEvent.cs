﻿using Prism.Events;
using Trackfiend.Client.Infrastructure.ValueObjects.Artists;

namespace Trackfiend.Client.Infrastructure.Events
{
    public class ArtistsEvent : PubSubEvent<object>
    {
    }
}
