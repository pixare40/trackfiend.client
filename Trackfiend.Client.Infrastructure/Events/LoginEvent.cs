﻿using Prism.Events;
using Trackfiend.Client.Common.Models.Account;

namespace Trackfiend.Client.Infrastructure.Events
{
    public class LoginEvent : PubSubEvent<LoginResponse>
    {
    }
}
