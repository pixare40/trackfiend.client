﻿using Prism.Events;

namespace Trackfiend.Client.Infrastructure.Events
{
    public class ArtistEvent : PubSubEvent<object>
    {
    }
}
