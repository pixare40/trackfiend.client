﻿using System;
using Prism;
using Prism.DryIoc;
using Prism.Ioc;
using Prism.Modularity;
using Trackfiend.Client.Infrastructure.Services;
using Trackfiend.Client.Modules.Articles.Services;
using Trackfiend.Client.Modules.Articles.ViewModels.Catalog;
using Trackfiend.Client.Modules.Articles.Views.Catalog;
using Trackfiend.Client.Modules.Profile;
using Trackfiend.Client.Modules.Profile.Services;
using Trackfiend.Client.Modules.Profile.ViewModels.Forms;
using Trackfiend.Client.Modules.Profile.Views.Forms;
using Trackfiend.Client.ViewModels;
using Trackfiend.Client.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Trackfiend.Client
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        //public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        public static string BaseImageUrl { get; } = "https://cdn.syncfusion.com/essential-ui-kit-for-xamarin.forms/common/uikitimages/";

        protected override async void OnInitialized()
        {
            string licenseKey = @"MzAzMTQ3QDMxMzgyZTMyMmUzMFo2OWFCYVJLVUUwdXVNSkpyaWpEbmlzUjRKSndDWHFyTytHM0xaWjk2SFE9";
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(licenseKey);
            InitializeComponent();

            await NavigationService.NavigateAsync("NavigationPage/SignUpPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterSingleton<ICommunicationsService, CommunicationsService>();
            containerRegistry.RegisterSingleton<IArticlesService, ArticlesService>();

            //containerRegistry.RegisterSingleton<ArtistService>();
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            base.ConfigureModuleCatalog(moduleCatalog);

            InitialiseProfileModule(moduleCatalog);
        }

        private static void InitialiseProfileModule(IModuleCatalog moduleCatalog)
        {
            Type profileModule = typeof(ProfileModule);
            moduleCatalog.AddModule(
              new ModuleInfo()
              {
                  ModuleName = profileModule.Name,
                  ModuleType = profileModule,
                  InitializationMode = InitializationMode.WhenAvailable
              });
        }
    }
}
