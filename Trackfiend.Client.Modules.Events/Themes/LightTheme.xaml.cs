﻿using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace Trackfiend.Client.Modules.Events.Themes
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LightTheme
    {
        public LightTheme()
        {
            InitializeComponent();
        }
    }
}