﻿using Prism.Ioc;
using Prism.Modularity;
using Trackfiend.Client.Modules.Events.ViewModels.Navigation;
using Trackfiend.Client.Modules.Events.Views;

namespace Trackfiend.Client.Modules.Events
{
    public class EventsModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<EventsPage, EventsPageViewModel>();
        }
    }
}
