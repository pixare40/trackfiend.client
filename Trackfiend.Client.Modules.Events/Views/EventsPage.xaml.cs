﻿using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace Trackfiend.Client.Modules.Events.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventsPage
    {
        public EventsPage()
        {
            InitializeComponent();
        }
    }
}