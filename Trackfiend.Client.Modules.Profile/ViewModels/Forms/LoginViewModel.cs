﻿using Prism.Events;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Trackfiend.Client.Infrastructure.ViewModels;
using Xamarin.Forms.Internals;

namespace Trackfiend.Client.Modules.Profile.ViewModels.Forms
{
    /// <summary>
    /// ViewModel for login page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class LoginViewModel : ViewModelBase
    {
        #region Fields

        private string email;

        private bool isInvalidEmail;

        private string errorMessage;

        private bool showErrorMessage;

        #endregion

        #region Property

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the email ID from user in the login page.
        /// </summary>
        public string Email
        {
            get
            {
                return this.email;
            }

            set
            {
                if (this.email == value)
                {
                    return;
                }

                ResetErrors();
                SetProperty(ref this.email, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the entered email is valid or invalid.
        /// </summary>
        public bool IsInvalidEmail
        {
            get
            {
                return this.isInvalidEmail;
            }

            set
            {
                if (this.isInvalidEmail == value)
                {
                    return;
                }

                SetProperty(ref this.isInvalidEmail, value);
            }
        }

        public string ErrorMessage
        {
            get { return this.errorMessage; }
            set
            {
                if(this.errorMessage == value)
                {
                    return;
                }

                SetProperty(ref this.errorMessage, value);
            }
        }

        public bool ShowErrorMessage
        {
            get { return showErrorMessage; }
            set
            {
                if(showErrorMessage == value)
                {
                    return;
                }

                SetProperty(ref showErrorMessage, value);
            }
        }

        private void ResetErrors()
        {
            ErrorMessage = null;
            ShowErrorMessage = false;
        }

        #endregion

        public LoginViewModel(IEventAggregator eventAggregator)
            :base(eventAggregator)
        {

        }
    }
}
