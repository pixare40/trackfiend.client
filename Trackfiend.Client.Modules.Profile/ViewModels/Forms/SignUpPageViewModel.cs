﻿using System;
using Prism.Commands;
using Prism.Events;
using Trackfiend.Client.Infrastructure.Models.Account;
using Trackfiend.Client.Infrastructure.Events;
using Trackfiend.Client.Modules.Profile.Services;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using GraphQL;
using Prism.Navigation;

namespace Trackfiend.Client.Modules.Profile.ViewModels.Forms
{
    /// <summary>
    /// ViewModel for sign-up page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class SignUpPageViewModel : LoginViewModel
    {
        #region Fields

        private readonly RegistrationService registrationService;

        private readonly INavigationService navigationService;

        private string name;

        private string password;

        private string confirmPassword;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="SignUpPageViewModel" /> class.
        /// </summary>
        public SignUpPageViewModel(IEventAggregator eventAggregator,
            RegistrationService registrationService, INavigationService navigationService)
            :base(eventAggregator)
        {
            this.LoginCommand = new DelegateCommand<object>(this.LoginClicked);
            this.SignUpCommand = new DelegateCommand<object>(this.SignUpClicked);
            this.registrationService = registrationService;
            this.navigationService = navigationService;
        }

        #endregion

        #region Property

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the name from user in the Sign Up page.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (this.name == value)
                {
                    return;
                }

                SetProperty(ref this.name, value);
            }
        }

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the password from users in the Sign Up page.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (this.password == value)
                {
                    return;
                }

                this.password = value;
                SetProperty(ref this.password, value);
            }
        }

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the password confirmation from users in the Sign Up page.
        /// </summary>
        public string ConfirmPassword
        {
            get
            {
                return this.confirmPassword;
            }

            set
            {
                if (this.confirmPassword == value)
                {
                    return;
                }

                this.confirmPassword = value;
                SetProperty(ref this.confirmPassword, value);
            }
        }

        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the command that is executed when the Log In button is clicked.
        /// </summary>
        public DelegateCommand<object> LoginCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the Sign Up button is clicked.
        /// </summary>
        public DelegateCommand<object> SignUpCommand { get; set; }

        #endregion

        protected override void SubscribeEvents()
        {
            base.SubscribeEvents();

            EventAggregator.GetEvent<RegistrationFailedEvent>().Subscribe(OnRegistrationFailed, ThreadOption.UIThread);
            EventAggregator.GetEvent<RegistrationSuccessEvent>().Subscribe(OnRegistrationSuccess, ThreadOption.UIThread);
        }

        protected override void UnsubscribeEvents()
        {
            base.UnsubscribeEvents();

            EventAggregator.GetEvent<RegistrationFailedEvent>().Unsubscribe(OnRegistrationFailed);
            EventAggregator.GetEvent<RegistrationSuccessEvent>().Unsubscribe(OnRegistrationSuccess);
        }

        private void OnRegistrationFailed(object[] obj)
        {
            GraphQLError[] errorObject = obj as GraphQLError[];
            string errorMessage = errorObject[0].Message;

            ErrorMessage = errorMessage;
            ShowErrorMessage = true;
        }

        private void OnRegistrationSuccess(RegistrationResponse obj)
        {
            //Redirect to articles
        }

        #region Methods

        /// <summary>
        /// Invoked when the Log in button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void LoginClicked(object obj)
        {
            await navigationService.NavigateAsync("NavigationPage/LoginPage");
        }

        /// <summary>
        /// Invoked when the Sign Up button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void SignUpClicked(object obj)
        {
            this.registrationService.RegisterUser(Name, Password, Email);
        }

        #endregion
    }
}