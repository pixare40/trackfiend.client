﻿using Prism.Ioc;
using Prism.Modularity;
using Trackfiend.Client.Modules.Profile.Services;
using Trackfiend.Client.Modules.Profile.ViewModels.Forms;
using Trackfiend.Client.Modules.Profile.Views.Forms;

namespace Trackfiend.Client.Modules.Profile
{
    public class ProfileModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<SignUpPage, SignUpPageViewModel>();
            containerRegistry.RegisterForNavigation<ForgotPasswordPage, ForgotPasswordViewModel>();
            containerRegistry.RegisterForNavigation<ResetPasswordPage, ResetPasswordViewModel>();

            containerRegistry.RegisterSingleton<RegistrationService>();
            containerRegistry.RegisterSingleton<ILoginService, LoginService>();
        }
    }
}
