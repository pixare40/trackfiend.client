﻿using System;
using GraphQL;
using Prism.Events;
using Trackfiend.Client.Infrastructure.Models.Account;
using Trackfiend.Client.Infrastructure.Events;
using Trackfiend.Client.Infrastructure.Services;
using Trackfiend.Client.Infrastructure.ValueObjects.Artists;

namespace Trackfiend.Client.Modules.Profile.Services
{
    public class RegistrationService
    {
        private readonly IEventAggregator eventAggregator;
        private readonly ICommunicationsService communicationsService;

        public RegistrationService(IEventAggregator eventAggregator,
            ICommunicationsService communicationsService)
        {
            this.eventAggregator = eventAggregator;
            this.communicationsService = communicationsService;
        }

        public async void RegisterUser(string name, string password, string email)
        {
            GraphQLResponse<RegistrationResponse> response =
                new GraphQLResponse<RegistrationResponse>();

            response = await communicationsService
            .SendQueryAsync<RegistrationResponse>(new AccountRequests()
            .Register(name, email, password));

            if(response == null)
            {
                return;
            }

            if (response.Errors != null)
            {
                eventAggregator.GetEvent<RegistrationFailedEvent>().Publish(response.Errors);
                return;
            }

            eventAggregator.GetEvent<RegistrationSuccessEvent>().Publish(response.Data);
        }
    }
}
