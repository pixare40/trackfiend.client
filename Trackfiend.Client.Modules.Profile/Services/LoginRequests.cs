﻿using System.Collections.Generic;
using GraphQL;

namespace Trackfiend.Client.Modules.Profile.Services
{
    public class LoginRequests
    {
        public GraphQLRequest LoginWithUserName(string email, string password) => new GraphQLRequest
        {
            Query=@"
            mutation Login($email: String!, $password: String!){
                login(email: $email, password: $password){
                    token
                }
            }",
            Variables = new
            {
                email,
                password
            }
        };
    }
}
