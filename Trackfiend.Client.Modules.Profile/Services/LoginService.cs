﻿using GraphQL;
using Prism.Events;
using Trackfiend.Client.Common.Models.Account;
using Trackfiend.Client.Infrastructure.Events;
using Trackfiend.Client.Infrastructure.Services;

namespace Trackfiend.Client.Modules.Profile.Services
{
    public class LoginService : ILoginService
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ICommunicationsService _communicationsService;

        public LoginService(IEventAggregator eventAggregator, ICommunicationsService communicationsService)
        {
            _eventAggregator = eventAggregator;
            _communicationsService = communicationsService;
        }

        public async void LoginWithUserName(string userName, string password)
        {
            GraphQLResponse<LoginResponse> response = await _communicationsService
                .SendQueryAsync<LoginResponse>(new LoginRequests()
                .LoginWithUserName(userName, password));

            if(response == null)
            {
                return;
            }

            if (response.Errors != null)
            {
                _eventAggregator.GetEvent<LoginFailedEvent>().Publish(response.Errors);
                return;
            }

            _eventAggregator.GetEvent<LoginEvent>().Publish(response.Data);
        }
    }
}
