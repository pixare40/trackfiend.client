﻿namespace Trackfiend.Client.Modules.Profile.Services
{
    public interface ILoginService
    {
        void LoginWithUserName(string email, string password);
    }
}